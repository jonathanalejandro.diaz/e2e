import { handlerPath } from '@commons/services';
// import { rantySnsTopicsConfig } from './triggers';

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  // events: [...rantySnsTopicsConfig]
};

class Triggers {
  static createSuscriptions(topicArns: string[], numberHelp: number) {
    let res = {};
    const regions = ['us-east-1', 'us-east-2'];
    for (let x = 0; x < topicArns.length; x++) {
      let sus = {
        Type: 'AWS::SNS::Subscription',
        Properties: {
          Endpoint: {
            'Fn::GetAtt': ['SaveTransactionSnsLambdaFunction', 'Arn']
          },
          Protocol: 'lambda',
          TopicArn: topicArns[x],
          Region: topicArns[x].includes(regions[0]) ? regions[0] : regions[1]
        }
      };
      res[`${numberHelp}${x}`] = sus;
    }
    return res;
  }
  static createPermissions(topicArns: string[], numberHelp: number) {
    let res = {};
    for (let x = 0; x < topicArns.length; x++) {
      let permission = {
        Type: 'AWS::Lambda::Permission',
        Properties: {
          FunctionName: {
            'Fn::GetAtt': ['SaveTransactionSnsLambdaFunction', 'Arn']
          },
          Action: 'lambda:InvokeFunction',
          Principal: 'sns.amazonaws.com',
          SourceArn: topicArns[x]
        }
      };
      res[`${numberHelp}${x}`] = permission;
    }
    return res;
  }
}
const pct = [
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-dynamicQr-wallet-transferPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-paymentLink-wallet-transferPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-staticQr-wallet-transferPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-wallet-transferPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-dynamicQr-wallet-transferPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-paymentLink-wallet-transferPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-staticQr-wallet-transferPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-wallet-transferPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-dynamicQr-wallet-transferPayment-pending',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-paymentLink-wallet-transferPayment-pending',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-staticQr-wallet-transferPayment-pending',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-wallet-transferPayment-pending',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-dynamicQr-wallet-transferPayment-pending',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-paymentLink-wallet-transferPayment-pending',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-staticQr-wallet-transferPayment-pending',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-wallet-transferPayment-pending'
];

const tctd = [
  'arn:aws:sns:us-east-1:585597747501:galicia-adquirencia-payments-update-topic',
  'arn:aws:sns:us-east-1:585597747501:galicia-adquirencia-payments-creation-topic',
  'arn:aws:sns:us-east-1:585597747501:nx-toque-payments-creation-topic',
  'arn:aws:sns:us-east-1:585597747501:nx-toque-payments-update-topic',
  'arn:aws:sns:us-east-2:585597747501:galicia-adquirencia-payments-creation-topic',
  'arn:aws:sns:us-east-2:585597747501:galicia-adquirencia-payments-update-topic',
  'arn:aws:sns:us-east-2:585597747501:nx-toque-payments-creation-topic',
  'arn:aws:sns:us-east-2:585597747501:nx-toque-payments-update-topic',

  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-cardOnFile-cardPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-staticQr-wallet-cardPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-manualInput-cardPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-wallet-cardPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-paymentLink-wallet-cardPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-paymentLink-manualInput-cardPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-dynamicQr-wallet-cardPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-webCheckout-manualInput-cardPayment',
  'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-clickToPay-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-cardOnFile-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-staticQr-wallet-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-manualInput-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-wallet-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-paymentLink-wallet-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-paymentLink-manualInput-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-dynamicQr-wallet-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-webCheckout-manualInput-cardPayment',
  'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-clickToPay-cardPayment'
];
tctd;
export const permission1 = {
  fullAccess1: {
    Type: 'AWS::Lambda::Permission',
    Properties: {
      FunctionName: {
        'Fn::GetAtt': ['SaveTransactionSnsLambdaFunction', 'Arn']
      },
      Action: 'lambda:InvokeFunction',
      Principal: 'sns.amazonaws.com',
      SourceArn: 'arn:aws:sns:us-east-1:845308403362:*'
    }
  }
};
export const permission2 = {
  fullAccess2: {
    Type: 'AWS::Lambda::Permission',
    Properties: {
      FunctionName: {
        'Fn::GetAtt': ['SaveTransactionSnsLambdaFunction', 'Arn']
      },
      Action: 'lambda:InvokeFunction',
      Principal: 'sns.amazonaws.com',
      SourceArn: 'arn:aws:sns:us-east-1:585597747501:*'
    }
  }
};
permission1;
export const suscriptionsTcp = Triggers.createSuscriptions(pct, 1);
export const permissionsTcp = Triggers.createPermissions(pct, 2);
export const suscriptionsTctd = Triggers.createSuscriptions(tctd, 3);
export const permissionsTctd = Triggers.createPermissions(tctd, 4);

export const permissionsAndSuscriptions = {
  ...suscriptionsTcp,
  ...suscriptionsTctd,
};
// console.log(JSON.stringify(permissionsAndSuscriptions, null,4))

export const SaveTransactioSnslambdaRole = {
  Type: 'AWS::IAM::Role',
  Properties: {
    AssumeRolePolicyDocument: {
      Version: '2012-10-17',
      Statement: [
        {
          Effect: 'Allow',
          Principal: {
            Service: 'lambda.amazonaws.com'
          },

          Action: 'sts:AssumeRole'
        }
      ]
    },
    Policies: [
      {
        PolicyName: {
          'Fn::Join': ['-', ['jona-marqa', 'develop', 'lambda']]
        },
        PolicyDocument: {
          Version: '2012-10-17',
          Statement: [
            {
              Effect: 'Allow',
              Action: ['logs:CreateLogStream', 'logs:CreateLogGroup', 'logs:PutLogEvents'],
              Resource: [
                {
                  'Fn::Sub':
                    'arn:${AWS::Partition}:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/jona-marqa-develop-saveTransactionSns:*:*'
                }
              ]
            },
            {
              Effect: 'Allow',
              Action: ['lambda:InvokeFunction', 'sns:Subscribe'],
              Resource: [
                /* 'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-dynamicQr-wallet-transferPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-paymentLink-wallet-transferPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-staticQr-wallet-transferPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-wallet-transferPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-dynamicQr-wallet-transferPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-paymentLink-wallet-transferPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-staticQr-wallet-transferPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-wallet-transferPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-dynamicQr-wallet-transferPayment-pending',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-paymentLink-wallet-transferPayment-pending',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-staticQr-wallet-transferPayment-pending',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-wallet-transferPayment-pending',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-dynamicQr-wallet-transferPayment-pending',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-paymentLink-wallet-transferPayment-pending',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-staticQr-wallet-transferPayment-pending', */
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-*'
              ]
            },
            {
              Effect: 'Allow',
              Action: ['dynamodb:GetItem', 'dynamodb:PutItem', 'dynamodb:Query'],
              Resource: {
                'Fn::ImportValue': 'OnboardedCrossServicesTableArn'
              }
            },
            {
              Effect: 'Allow',
              Action: ['dynamodb:PutItem'],
              Resource: { 'Fn::GetAtt': ['RawData', 'Arn'] }
            }
          ]
        }
      },

      //---------------second policy
      {
        PolicyName: {
          'Fn::Join': ['-', ['jona-marqa2', 'develop', 'lambda']]
        },
        PolicyDocument: {
          Version: '2012-10-17',
          Statement: [
            {
              Effect: 'Allow',
              Action: ['lambda:InvokeFunction', 'sns:Subscribe'],
              Resource: [
                'arn:aws:sns:us-east-1:585597747501:galicia-adquirencia-payments-update-topic'
                /*  'arn:aws:sns:us-east-1:585597747501:galicia-adquirencia-payments-creation-topic',
                'arn:aws:sns:us-east-1:585597747501:nx-toque-payments-creation-topic',
                'arn:aws:sns:us-east-1:585597747501:nx-toque-payments-update-topic',
                'arn:aws:sns:us-east-2:585597747501:galicia-adquirencia-payments-creation-topic',
                'arn:aws:sns:us-east-2:585597747501:galicia-adquirencia-payments-update-topic',
                'arn:aws:sns:us-east-2:585597747501:nx-toque-payments-creation-topic',
                'arn:aws:sns:us-east-2:585597747501:nx-toque-payments-update-topic',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-cardOnFile-cardPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-staticQr-wallet-cardPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-manualInput-cardPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-wallet-cardPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-paymentLink-wallet-cardPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-paymentLink-manualInput-cardPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-dynamicQr-wallet-cardPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-webCheckout-manualInput-cardPayment',
                'arn:aws:sns:us-east-1:845308403362:ranty-sls-components-develop-ecommerce-clickToPay-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-cardOnFile-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-staticQr-wallet-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-manualInput-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-ecommerce-wallet-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-paymentLink-wallet-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-paymentLink-manualInput-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-dynamicQr-wallet-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-webCheckout-manualInput-cardPayment',
                'arn:aws:sns:us-east-2:845308403362:ranty-sls-components-develop-*' */
              ]
            }
          ]
        }
      }
    ]
  }
};
export const SaveTransactionSnsLogGroup = {
  Type: 'AWS::Logs::LogGroup',
  Properties: {
    LogGroupName: '/aws/lambda/jona-marqa-develop-saveTransactionSns',
    Tags: [
      {
        Key: 'owner',
        Value: 'nx.technology.merchants.sonqo.ongoing-processes'
      },
      { Key: 'vertical-name', Value: 'merchants-development' },
      { Key: 'domain-name', Value: 'sonqo' },
      { Key: 'squad-name', Value: 'ongoing-processes' },
      { Key: 'system-name', Value: 'marqa' },
      { Key: 'shared-resource', Value: 'false' },
      { Key: 'gitlab-id', Value: '48493701' },
      { Key: 'environment', Value: 'dev' },
      { Key: 'provider', Value: 'aws' },
      { Key: 'data-type-bkp', Value: 'temporal-data' }
    ]
  }
};

import { env } from 'process';
const baseName = 'saveTransactionSns';
// const slsName = 'RawDataSqs';
const name = env.STACK_NAME + 'x-' + env.STAGE + '-' + baseName;

const metadata = {
  baseName,
  name
};

export const JonaSaveTransactionSnsResourceFunction = {
  Type: 'AWS::Lambda::Function',
  Properties: {
    Code: {
      S3Bucket: { Ref: 'ServerlessDeploymentBucket' },
      S3Key: 'serverless/jona-marqa/develop/1715818091873-2024-05-16T00:08:11.873Z/saveTransactionSns.zip'
    },
    //file
    FunctionName: metadata.name,
    Handler: `${handlerPath(__dirname)}/handler.main`,
    Runtime: 'nodejs18.x',
    MemorySize: 1024,
    Timeout: 6,
    Environment: {
      Variables: {
        STACK_NAME: 'jona-marqa',
        STACK_PREFIX: 'jona',
        PROJECT_NAME: 'marqa',
        STAGE: 'develop',
        REGION: 'us-east-1',
        APP_VERSION: '3.2.11',
        OWNER_PROJECT_NAME: 'sonqo',
        OBSERVABILITY_OWNER: 'ongoing-processes',
        OBSERVABILITY_VERTICAL: 'Merchants',
        SERVICE_ENVIRONMENT: 'dev',
        DEFAULT_CONSUMER_ID: 'marqa',
        AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
        NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
        OFFLINE_RUNNING: 'false',
        ONBOARDED_CROSS_SERVICES_TABLE_NAME: {
          'Fn::ImportValue': 'OnboardedCrossServicesTableName'
        },
        ONBOARDED_CROSS_SERVICES_TABLE_ARN: {
          'Fn::ImportValue': 'OnboardedCrossServicesTableArn'
        },

        RAW_DATA_STREAM_ARN: { 'Fn::GetAtt': ['RawData', 'StreamArn'] },
        RAW_DATA_TABLE_NAME: { Ref: 'RawData' },
        FilterDestinationName:
          'arn:aws:firehose:us-east-1:239574588374:deliverystream/develop-kinesis-firehose-datadog-DatadogDeliveryStream',
        FilterRoleName: 'arn:aws:iam::239574588374:role/develop-kinesis-firehose-datadog-CloudWatchLogsRole-novi-op',
        API_DOMAIN_NAME: 'e3-marqa.sonqo.io',
        MARQA_AWS_ACCOUNT_ID: '585597747501',
        RANTY_AWS_ACCOUNT_ID: '845308403362'
      }
    },
    Tags: [
      {
        Key: 'owner',
        Value: 'nx.technology.merchants.sonqo.ongoing-processes'
      },
      { Key: 'vertical-name', Value: 'merchants-development' },
      { Key: 'domain-name', Value: 'sonqo' },
      { Key: 'squad-name', Value: 'ongoing-processes' },
      { Key: 'system-name', Value: 'marqa' },
      { Key: 'shared-resource', Value: 'false' },
      { Key: 'gitlab-id', Value: '48493701' },
      { Key: 'environment', Value: 'dev' },
      { Key: 'provider', Value: 'aws' },
      { Key: 'data-type-bkp', Value: 'temporal-data' }
    ],
    Role: { 'Fn::GetAtt': ['SaveTransactioSnslambdaRole', 'Arn'] },
    LoggingConfig: {
      LogGroup: '/aws/lambda/jona-marqa-develop-saveTransactionSns'
    }

    /* Events:[
      MySnsEvent1:{
        Type: 'SNS',
        Properties:
          Topic: arn:aws:sns:us-east-1:123456789012:topic1
      }


    MySnsEvent2:
      Type: SNS
      Properties:
        Topic: arn:aws:sns:us-east-1:123456789012:topic2
    ] */
  },
  DependsOn: ['SaveTransactioSnslambdaRole', 'SaveTransactionSnsLogGroup']
};
