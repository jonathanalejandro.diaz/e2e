const help = {
  "payment": {
    "id": "14789340-4824-4718-81ac-5a47676c7426",
    "application": {
      "name": "CardPayments",
      "id": "00dd46a3-ec3b-4637-934b-08570f3f917d",
      "type": "INTEGRATOR",
      "client_id": "HgUaYbqwNJkQ8FjfBss6HT3T9ikqjWbV"
    },
    "acquirer_id": "",
    "partner_id": "galicia_adquirencia",
    "payment_type": "ecommerce",
    "creation_date": "2024-06-11T15:59:51.921Z",
    "status": "REJECTED",
    "status_reason": { "description": "Gateway timeout", "code": "gateway_timeout" },
    "merchant_id": "galicia_adquirencia#101",
    "client_id": "HgUaYbqwNJkQ8FjfBss6HT3T9ikqjWbV",
    "external_payment_id": "7ba55d60-9534-41b4-b6ff-df10564374d0",
    "payment_method": {
      "type": "card_payment",
      "card": { "masked_pan": "", "type": "", "brand": "", "name": "NOT_SENDED" },
      "holder": {
        "name": "agus de moro",
        "doc_number": "42022645",
        "doc_type": "DNI",
        "email": "agusdemoro@gmail.com"
      },
      "data_privacy": { "encrypted": { "origin": "non_present" } },
      "installment_plan": {
        "id": "66905a79-9a56-4664-bea2-7e78d501b837",
        "installments_plan_gateway_id": "arg-bank-01",
        "name": "1 CUOTA",
        "installments": 1,
        "total_amount": { "value": "13.00", "currency": "ARS" },
        "interest_rate": "0.00"
      }
    },
    "transactions": [
      {
        "id": "b6bd770c-11d6-44a3-8151-f0fe4dd1e221",
        "type": "PURCHASE",
        "status": "REJECTED",
        "creation_date": "2024-06-11T15:59:51.921Z",
        "auth_data": {
          "id": "",
          "code": "gateway_timeout",
          "reason": "Rejected (Gateway timeout)",
          "status": "REJECTED",
          "operation_id": "",
          "description": "Gateway timeout",
          "ticket": { "batch": "", "number": "" },
          "auth_id": "000000",
          "acquirer_ref_number": "0",
          "system_datetime": ""
        },
        "products": [
          {
            "name": "Pago online Nacho",
            "description": "Pago online Nacho",
            "id": "883627",
            "quantity": 1,
            "unit_price": { "value": "13.00", "currency": "ARS" }
          },
          {
            "name": "Pago online Nacho",
            "description": "Pago online Nacho",
            "id": "883627",
            "quantity": 2,
            "unit_price": { "value": "13.00", "currency": "ARS" }
          },
          {
            "name": "Pago online Nacho",
            "description": "Pago online Nacho",
            "id": "883627",
            "quantity": 4,
            "unit_price": { "value": "13.00", "currency": "ARS" }
          },
          {
            "name": "Pago online Nacho",
            "description": "Pago online Nacho",
            "id": "883627",
            "quantity": 5,
            "unit_price": { "value": "13.00", "currency": "ARS" }
          }
        ],
        "currency": "ARS",
        "amount": "13.00"
      }
    ],
    "seller": {
      "store_id": "6bfd6685-fee7-482d-b4a8-0f0c33c0618d",
      "country": "ARG",
      "fantasy_name": "Telemetricos palermoo",
      "company_id": "comp_2",
      "store_data": {
        "country": "ARG",
        "address": { "number": "3086", "street": "Arenales", "notes": '\'\'\'\'00$#""$"$"!%%#"&' },
        "fantasy_name": "Telemetricos palermoo",
        "updated_at": "2023-07-05T21:13:21.368Z",
        "city": "CIUDAD AUTONOMA BUENOS AIRES",
        "id": "6bfd6685-fee7-482d-b4a8-0f0c33c0618d",
        "state": "C",
        "mcc": "5964",
        "zip_code": "C1425BEL"
      },
      "city": "CIUDAD AUTONOMA BUENOS AIRES",
      "owner_id": "31f41700-af94-4f66-b8f8-4a23d3f70bd1",
      "owner_data": {
        "business_name": "DANCAN S A",
        "country": "ARG",
        "address": { "number": "3086", "street": "Arenales", "notes": '\'\'\'\'00$#""$"$"!%%#"&' },
        "city": "CIUDAD AUTONOMA BUENOS AIRES",
        "tax_registration_date": "2023-02-09",
        "external_id": "18599",
        "zip_code": "C1425BEL",
        "tax_id": "30521130314",
        "updated_at": "2023-07-05T21:13:20.692Z",
        "tax_id_type": "arg.cuit",
        "id": "31f41700-af94-4f66-b8f8-4a23d3f70bd1",
        "state": "C",
        "owner_type": "ARG_LEGAL_PERSON"
      },
      "store_address": {
        "zipcode": "C1425BEL",
        "number": "3086",
        "country": "ARG",
        "notes": '\'\'\'\'00$#""$"$"!%%#"&',
        "city": "CIUDAD AUTONOMA BUENOS AIRES",
        "street": "Arenales",
        "region": "C"
      },
      "mcc": "5964",
      "tax_id": "30521130314",
      "payment_type": "ecommerce",
      "category_id": "5964",
      "branch_id": "1c9b2c6c-18a2-4b77-a6e0-3c7dd89a5af0",
      "user_id": "18599",
      "tax_id_type": "arg.cuit",
      "pos_id": "b179b1ba-0c13-4862-9e17-1386847be771",
      "name": "Telemetricos palermoo",
      "company_data": {
        "fraud_prevention_config": {
          "skip_fraud_service": false,
          "services": [
            { "priority": 0, "config_id": "galicia", "acquirer_id": "02", "service_id": "mastercard" },
            { "priority": 1, "config_id": "galicia", "acquirer_id": "02", "service_id": "cybersource" }
          ]
        },
        "seller": { "callback_url": "https://ycer-experience-qas.gab.bancogalicia.com.ar/api/v1/payment/callback" },
        "id": "comp_2"
      },
      "legal_name": "DANCAN S A",
      "branch_data": {
        "country": "ARG",
        "address": { "number": "3086", "street": "Arenales", "notes": '\'\'\'\'00$#""$"$"!%%#"&' },
        "city": "CIUDAD AUTONOMA BUENOS AIRES",
        "name": "TIENDANUBE",
        "id": "1c9b2c6c-18a2-4b77-a6e0-3c7dd89a5af0",
        "state": "C",
        "zip_code": "C1425BEL"
      },
      "onboarding_date": "2023-02-09"
    },
    "buyer": {
      "user_email": "agusdemoro@gmail.com",
      "billing_address": {
        "zipcode": "5000",
        "number": "",
        "country": "AR",
        "notes": "",
        "city": "1",
        "street": "",
        "region": "Buenos Aires"
      },
      "doc_type": "DNI",
      "user_id": "agusdemoro@gmail.c"
    }
  }
};
