import * as cdk from "@aws-cdk/core";
import * as sns from "@aws-cdk/aws-sns";
import * as lambda from "@aws-cdk/aws-lambda";
import { LambdaSubscription } from "@aws-cdk/aws-sns-subscriptions";

class MyStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Assuming you have ARNs or names of existing Lambda function and SNS topic
    const existingLambdaArn =
      "arn:aws:lambda:us-east-1:239574588374:function:jona-marqa-develop-saveTransactionSns";
    const existingTopicArn =
      "arn:aws:sns:us-east-1:585597747501:galicia-adquirencia-payments-creation-topic";

    // Import the existing Lambda function and SNS topic
    const existingLambda = lambda.Function.fromFunctionArn(
      this,
      "ExistingLambda",
      existingLambdaArn
    );
    const existingTopic = sns.Topic.fromTopicArn(
      this,
      "ExistingTopic",
      existingTopicArn
    );

    // Subscribe the existing Lambda function to the existing SNS topic
    existingTopic.addSubscription(new LambdaSubscription(existingLambda));
  }
}

const suscription = () => {
  try {
    const app = new cdk.App();
    new MyStack(app, "jona-marqa-develop");
    const res = app.synth();
    console.log("-----res", );
  } catch (error) {
    console.log("------error", error);
  }
};
suscription();
