import { SNS } from "aws-sdk";
// import { Handler } from 'aws-lambda';

const sns = new SNS({ region: "us-east-1" }); // Replace 'YOUR_REGION' with your AWS region

export const handler = async () => {
  try {
    // Subscribe Lambda function to existing SNS topic
    const subscribeParams: SNS.SubscribeInput = {
      Protocol: "lambda",
      TopicArn:
        "arn:aws:sns:us-east-1:585597747501:galicia-adquirencia-payments-creation-topic",
      Endpoint: "arn:aws:lambda:us-east-1:239574588374:function:jona-marqa-develop-saveTransactionSns", // Subscribe Lambda function itself
    };

    const res = await sns.subscribe(subscribeParams).promise();
    console.log(res);
    console.log("Lambda function subscribed to the SNS topic successfully.");

    return { statusCode: 200, body: "Subscription successful" };
  } catch (error) {
    console.error("Error subscribing to SNS topic:", error);
    return { statusCode: 500, body: "Subscription failed" };
  }
};
handler();
