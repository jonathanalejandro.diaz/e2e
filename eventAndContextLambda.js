const event = {
    Records: [
      {
        messageId: '3e805acc-3d06-44ca-94a6-b0db1560d123',
        receiptHandle: 'AQEBKRMH3Rd139VSTNdGc/P1onca+XPf33aBeTSsReDIDrwSzVvfGl1thFxTkXa8NhQb2JC0oXZcc4k1yBGKt8IyUhPmuW4VnbbQRPQWt7Pf3F97AZqF5RJ366u7GyOiEQAd3VeZAwxqN+iLboJnwDxJxeLEwC4e00r7L++BH07FCq4Np5PbhFdPmgGRk2J5YRqd+o1a9PBNr4M94WQFIwTCBHvSpwZwVH+67aPouWLmV/ZIU99Mf592+9lkwXeYmFnBDQq9hYZgDAC+tBfys9iIqstc7lusqQqhRcruSQlD8nSAa7IKdf9GmvnnJNCV169qrmAuB/nCnvgvDchg5XInu2dwIit3CQsOqoELzWbdaRelziEhNZL9YSZYKAFfRGmCTpWBWm9eJ7+SM6z/UqQRLd5Ap/fAyfqDvrJPpZf6rzU=',
        body: '[{"workflowName":"testSns","caseName":"happyPath","expectedResult":{"workflowStatus":"completed","lastWorkflowStatus":"pending","testSnsActorA":true,"testSnsActorB":true,"testSnsActorC":true},"verificationIds":{"testSnsActorA":{"commonIdAB":"45c464d0-a217-4d40-aac7-0e3293b75c83"},"testSnsActorB":{"commonIdAB":"45c464d0-a217-4d40-aac7-0e3293b75c83","commonIdBC":"5ee34d5a-a3c2-407f-b50a-4e4ed25d5d7a"},"testSnsActorC":{"commonIdBC":"5ee34d5a-a3c2-407f-b50a-4e4ed25d5d7a"}}},{"workflowName":"testSns","caseName":"stressed","expectedResult":{"workflowStatus":"completed","lastWorkflowStatus":"pending","testSnsActorA":true,"testSnsActorB":true,"testSnsActorC":true},"verificationIds":{"testSnsActorA":{"commonIdAB":"46dc11bc-fc8b-461c-b177-dc41143ae4f4"},"testSnsActorB":{"commonIdAB":"46dc11bc-fc8b-461c-b177-dc41143ae4f4","commonIdBC":"ef759b93-315b-44d7-b47f-7404befd7396"},"testSnsActorC":{"commonIdBC":"ef759b93-315b-44d7-b47f-7404befd7396"}}},{"workflowName":"testSns","caseName":"slow","expectedResult":{"workflowStatus":"completed","lastWorkflowStatus":"incomplete","testSnsActorA":true,"testSnsActorB":true,"testSnsActorC":true},"verificationIds":{"testSnsActorA":{"commonIdAB":"64dd97d3-ef20-474b-8a66-c0933d949a35"},"testSnsActorB":{"commonIdAB":"64dd97d3-ef20-474b-8a66-c0933d949a35","commonIdBC":"b980db90-273d-4779-86d3-7975ce59cf53"},"testSnsActorC":{"commonIdBC":"b980db90-273d-4779-86d3-7975ce59cf53"}}},{"workflowName":"testSns","caseName":"missing","expectedResult":{"workflowStatus":"incomplete","lastWorkflowStatus":"pending","testSnsActorA":true,"testSnsActorB":false,"testSnsActorC":true},"verificationIds":{"testSnsActorA":{"commonIdAB":"68044ab6-2c0f-4e4e-85c3-5f785e0968fd"},"testSnsActorC":{"commonIdBC":"39277f83-cb61-46f6-ab28-3bd4befcf555"}}},{"workflowName":"testSns","caseName":"messy","expectedResult":{"workflowStatus":"completed","lastWorkflowStatus":"pending","testSnsActorA":true,"testSnsActorB":true,"testSnsActorC":true},"verificationIds":{"testSnsActorB":{"commonIdAB":"526ca6ad-9ccd-4d15-a67e-9ead07e17fd2","commonIdBC":"d811db43-18b2-4193-be94-e672b749d7b2"},"testSnsActorA":{"commonIdAB":"526ca6ad-9ccd-4d15-a67e-9ead07e17fd2"},"testSnsActorC":{"commonIdBC":"d811db43-18b2-4193-be94-e672b749d7b2"}}}]',
        attributes: [Object],
        messageAttributes: {},
        md5OfBody: '67297eb9d3c516553792acb0f9dbabbe',
        eventSource: 'aws:sqs',
        eventSourceARN: 'arn:aws:sqs:us-east-1:239574588374:jona-marqa-develop-initialzed-tests-sqs',
        awsRegion: 'us-east-1'
      }
    ]
  }

  const context = {
    callbackWaitsForEmptyEventLoop: ['Getter/Setter'],
    succeed: [Function ('anonymous')],
    fail: [Function ('anonymous')],
    done: [Function ('anonymous')],
    functionVersion: '$LATEST',
    functionName: 'jona-marqa-develop-testsRunner',
    memoryLimitInMB: '1024',
    logGroupName: '/aws/lambda/jona-marqa-develop-testsRunner',
    logStreamName: '2024/04/21/[$LATEST]bc2a86ae0c084ce9a4a5dd548e476939',
    clientContext: undefined,
    identity: undefined,
    invokedFunctionArn: 'arn:aws:lambda:us-east-1:239574588374:function:jona-marqa-develop-testsRunner',
    awsRequestId: '61349e3a-8001-58c9-9876-c0a5bd07a080',
    getRemainingTimeInMillis: [Function ('getRemainingTimeInMillis')]
  } 