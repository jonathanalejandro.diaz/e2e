const workflow = {
  "workflowName": "pct",
  "branchesConfig": {
    "mainApproved": {
      "coelsaAdapter": {
        "mustExcludeTags": [
          [
            "REFUNDED"
          ]
        ],
        "mustIncludeTags": [
          "all",
          [
            "comp_2",
            "comp_7"
          ]
        ]
      },
      "coreAccreditation": {
        "mustExcludeTags": [
        ],
        "mustIncludeTags": [
          "all",
          "ACCREDITED"
        ]
      },
      "paymentApi": {
        "mustExcludeTags": [
        ],
        "mustIncludeTags": [
          "all",
          [
            "comp_2",
            "comp_7"
          ],
          "APPROVED"
        ]
      },
      "sellerExperience": {
        "mustExcludeTags": [
        ],
        "mustIncludeTags": [
          "all",
          "APPROVED"
        ]
      }
    },
    "mainRejected": {
      "coelsaAdapter": {
        "mustExcludeTags": [
          [
            "REFUNDED"
          ]
        ],
        "mustIncludeTags": [
          "all",
          [
            "comp_2",
            "comp_7"
          ]
        ]
      },
      "coreAccreditation": {
        "mustExcludeTags": [
          "all"
        ],
        "mustIncludeTags": [
        ]
      },
      "paymentApi": {
        "mustExcludeTags": [
        ],
        "mustIncludeTags": [
          "all",
          [
            "comp_2",
            "comp_7"
          ],
          "REJECTED"
        ]
      },
      "sellerExperience": {
        "mustExcludeTags": [
        ],
        "mustIncludeTags": [
          "all",
          "REJECTED"
        ]
      }
    }
  },
  "createdAt": "2024-02-07T12:06:49.062Z",
  "onboardingSourcesNames": [
  ],
  "reconciliationNames": [
    "coelsaAdapter-coreAccreditation",
    "sellerExperience-coreAccreditation",
    "paymentApi-coreAccreditation",
    "coelsaAdapter-paymentApi",
    "coelsaAdapter-sellerExperience",
    "paymentApi-sellerExperience"
  ],
  "sourceNames": [
    "coreAccreditation",
    "coelsaAdapter",
    "paymentApi",
    "sellerExperience"
  ],
  "updatedAt": "2024-10-21T08:00:00.062Z",
  "waitingTimeSecsToClose": 900
}
