const tcTd = {
    "workflowName": "tcTd",
    "branchesConfig": {
        "devicesApproved": {
            "coreAccreditation": {
                "mustExcludeTags": [
                ],
                "mustIncludeTags": [
                    "all",
                    "ACCREDITED"
                ]
            },
            "paymentApi": {
                "mustExcludeTags": [
                    "all"
                ],
                "mustIncludeTags": [
                ]
            },
            "paymentProcessingTreatment": {
                "mustExcludeTags": [
                ],
                "mustIncludeTags": [
                    "all",
                    "APPROVED"
                ]
            },
            "punku": {
                "mustExcludeTags": [
                ],
                "mustIncludeTags": [
                    "all",
                    "smart_pos",
                    "APPROVED"
                ]
            },
            "transactionApiCreate": {
                "mustExcludeTags": [
                    "test_partner"
                ],
                "mustIncludeTags": [
                    "all",
                    [
                        "galicia_adquirencia.emv",
                        "galicia_adquirencia.swipe"
                    ]
                ]
            },
            "transactionApiUpdate": {
                "mustExcludeTags": [
                    "test_partner"
                ],
                "mustIncludeTags": [
                    "all",
                    [
                        "galicia_adquirencia.emv",
                        "galicia_adquirencia.swipe"
                    ]
                ]
            }
        },
        "devicesRejected": {
            "paymentApi": {
                "mustExcludeTags": [
                    "all"
                ],
                "mustIncludeTags": [
                ]
            },
            "paymentProcessingTreatment": {
                "mustExcludeTags": [
                ],
                "mustIncludeTags": [
                    "all",
                    "REJECTED"
                ]
            },
            "punku": {
                "mustExcludeTags": [
                ],
                "mustIncludeTags": [
                    "all",
                    "smart_pos",
                    "REJECTED"
                ]
            },
            "transactionApiCreate": {
                "mustExcludeTags": [
                    "test_partner"
                ],
                "mustIncludeTags": [
                    "all",
                    [
                        "galicia_adquirencia.emv",
                        "galicia_adquirencia.swipe"
                    ]
                ]
            },
            "transactionApiUpdate": {
                "mustExcludeTags": [
                    "test_partner"
                ],
                "mustIncludeTags": [
                    "all",
                    [
                        "galicia_adquirencia.emv",
                        "galicia_adquirencia.swipe"
                    ]
                ]
            }
        },
        "mainApproved": {
            "coreAccreditation": {
                "mustExcludeTags": [
                ],
                "mustIncludeTags": [
                    "all",
                    "ACCREDITED"
                ]
            },
            "paymentApi": {
                "mustExcludeTags": [
                    [
                        "payzen",
                        "risky_payment"
                    ]
                ],
                "mustIncludeTags": [
                    "all",
                    "APPROVED"
                ]
            },
            "paymentProcessingTreatment": {
                "mustExcludeTags": [
                ],
                "mustIncludeTags": [
                    "all",
                    "APPROVED"
                ]
            },
            "punku": {
                "mustExcludeTags": [
                    "all"
                ],
                "mustIncludeTags": [
                ]
            },
            "transactionApiCreate": {
                "mustExcludeTags": [
                    [
                        "galicia_adquirencia.emv",
                        "galicia_adquirencia.swipe",
                        "test_partner"
                    ]
                ],
                "mustIncludeTags": [
                    "all"
                ]
            },
            "transactionApiUpdate": {
                "mustExcludeTags": [
                    "galicia_adquirencia.emv",
                    "galicia_adquirencia.swipe",
                    "test_partner"
                ],
                "mustIncludeTags": [
                    "all"
                ]
            }
        },
        "mainRejected": {
            "paymentApi": {
                "mustExcludeTags": [
                    [
                        "payzen",
                        "risky_payment"
                    ]
                ],
                "mustIncludeTags": [
                    "all",
                    "REJECTED"
                ]
            },
            "paymentProcessingTreatment": {
                "mustExcludeTags": [
                ],
                "mustIncludeTags": [
                    "all",
                    "REJECTED"
                ]
            },
            "punku": {
                "mustExcludeTags": [
                    "all"
                ],
                "mustIncludeTags": [
                ]
            },
            "transactionApiCreate": {
                "mustExcludeTags": [
                    "galicia_adquirencia.emv",
                    "galicia_adquirencia.swipe",
                    "test_partner"
                ],
                "mustIncludeTags": [
                    "all"
                ]
            },
            "transactionApiUpdate": {
                "mustExcludeTags": [
                    "galicia_adquirencia.emv",
                    "galicia_adquirencia.swipe",
                    "test_partner"
                ],
                "mustIncludeTags": [
                    "all"
                ]
            }
        }
    },
    "createdAt": "2023-10-10T12:06:49.062Z",
    "onboardingSourcesNames": [
        "punku"
    ],
    "reconciliationNames": [
        "punku-coreAccreditation",
        "transactionApiCreate-coreAccreditation",
        "transactionApiUpdate-coreAccreditation",
        "paymentProcessingTreatment-coreAccreditation",
        "paymentApi-coreAccreditation",
        "paymentApi-transactionApiCreate",
        "transactionApiCreate-transactionApiUpdate",
        "paymentApi-transactionApiUpdate",
        "paymentApi-punku",
        "punku-transactionApiCreate",
        "punku-transactionApiUpdate",
        "paymentApi-paymentProcessingTreatment",
        "paymentProcessingTreatment-punku",
        "paymentProcessingTreatment-transactionApiCreate",
        "paymentProcessingTreatment-transactionApiUpdate"
    ],
    "sourceNames": [
        "paymentApi",
        "punku",
        "transactionApiCreate",
        "transactionApiUpdate",
        "paymentProcessingTreatment",
        "coreAccreditation"
    ],
    "updatedAt": "2024-10-21T08:00:00.062Z",
    "waitingTimeSecsToClose": 900
}